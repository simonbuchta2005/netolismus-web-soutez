import React from 'react'
import { Box, Button, Divider, Drawer, DrawerBody, DrawerCloseButton, DrawerContent, DrawerHeader, DrawerOverlay, Flex, IconButton, Image, MenuIcon, Slide, SlideFade, Text, useColorMode, useDisclosure, useMediaQuery } from '@chakra-ui/react'
import { HamburgerIcon, MoonIcon, SunIcon } from '@chakra-ui/icons';
import { useRouter } from 'next/router';
import { FaFacebook } from 'react-icons/fa';

interface MainLayoutProps {
  children: JSX.Element
}

interface Sections {
  id: string,
  title: string,
}

const MainLayout: React.FC<MainLayoutProps> = ({ children }) => {
  const { colorMode, toggleColorMode } = useColorMode();
  const { isOpen, onOpen, onClose } = useDisclosure()
  const [shouldHamburgerBeVisible] = useMediaQuery("(min-width: 768px)");

  const scrollToItem = (i: string, location: ScrollLogicalPosition) => {
    const item = document.querySelector(`#${i}`)
    if (!item) {
        return
    }
    item.scrollIntoView({ 
        behavior: 'smooth',
        inline: 'nearest',
        block: location,
    })
    onClose()
  }

  const sections: Sections[] = [
    {
      id: 'types',
      title: 'Typy',
    },
    {
      id: 'characters',
      title: 'Znaky',
    },
    {
      id: 'stats',
      title: 'Statistiky',
    },
  ]

  return (
    <Box
      width='100%'
    >
      <Flex
        width='100%'
        justifyContent='center'
      >
        <Box
          as='header'
          position='absolute'
          top='25px'
          width={{ base: '90%', xl: '1100px' }}
          zIndex='100'
          py='10px'
          borderRadius="1rem"
        >
          <Flex
            alignItems='center'
            justifyContent='space-between'
          >
              <SlideFade in={true} delay={0.025 + 0.75} offsetX="-20px" offsetY="0">
                <Flex
                  alignItems='center'
                >
                  <Image
                    alt='globe' 
                    src='/globe.svg' 
                    width='30px' 
                    height='30px'
                    bgGradient='linear(to-r, #D74177, #FFE98A)'
                    p='5px'
                    borderRadius='50%'
                    mr='5px'
                  />
                  <Text
                    fontWeight='bold'
                    bgGradient='linear(to-r, #D74177, #FFE98A)'
                    bgClip='text'
                    fontSize='xl'
                  >
                    Netolismus
                  </Text>
                </Flex>
              </SlideFade>
            <Flex>
              {
                shouldHamburgerBeVisible ? (
                  sections.map((e, idx) => 
                  <SlideFade in={true} offsetY="-20px" delay={(idx + 1) * 0.025 + 0.75}>
                    <Button 
                      ml='10px' 
                      size='lg' 
                      variant='ghost'
                      color='blackAlpha.800'
                      key={idx}
                      onClick={() => scrollToItem(e.id, 'start')}
                      _hover={{
                        bgGradient: 'linear(to-r, #D74177, #FFE98A)',
                        bgClip: 'text'
                      }}
                    >
                      {e.title}
                    </Button>
                  </SlideFade>
                  )
                ) : (
                  <IconButton icon={<HamburgerIcon />} aria-label='Menu' onClick={() => onOpen()} />
                )
              }
            </Flex>
          </Flex>
        </Box>
      </Flex>
      <Box
        width='100%'
      >
        {children}
      </Box>
      <Image
        src='/wave-top.svg'
      />
      <Flex
        bgGradient='linear(to-t, #D74177, #FFE98A)'
        height='250px'
        justifyContent='center'
      >
        <Flex
          width='400px'
          flexFlow='column'
          justifyContent='center'
          height='100%'
        >
          <Text
            color='white'
            fontSize='5xl'
            fontWeight='bold'
            textAlign='left'
            textShadow='2px 4px 4px rgba(0,0,0,0.2),
            0px -5px 10px rgba(255,255,255,0.15)'
          >
            "Dávka dělá jed."
          </Text>
          <Text
            color='white'
            fontSize='3xl'
            fontWeight='medium'
            textAlign='right'
            textShadow='2px 4px 4px rgba(0,0,0,0.2),
            0px -5px 10px rgba(255,255,255,0.15)'
          >
            - Paracelsus
          </Text>
        </Flex>
      </Flex>
      <Image
        src='/wave-bottom.svg'
        boxShadow='2xl'
      />
      <Flex
        id='help'
        my='50px'
        justifyContent='center'
      >
        <Flex
          width='1000px'
        >
          <Flex
            width='100%'
          >
            <Flex
              width='50%'
              flexFlow='column'
              gap='10px'
            >
              <Text mt='10px' fontSize='md' color='blackAlpha.700'>Pořebujete odbornou pomoc?</Text>
              <Button width='150px' size='lg' onClick={() => window.open('https://www.facebook.com/EBezpeci/', '_blank')} colorScheme='facebook' leftIcon={<FaFacebook/>}>E-Bezpečí</Button>
            </Flex>
            <Flex
              width='50%'
              flexFlow='column'
            >
              {sections.map((e, idx) => 
              <SlideFade in={true} offsetY="-20px" delay={(idx + 1) * 0.025 + 0.75}>
                <Button 
                  ml='10px' 
                  size='lg' 
                  variant='ghost'
                  color='blackAlpha.700'
                  key={idx}
                  onClick={() => scrollToItem(e.id, 'start')}
                  _hover={{
                    bgGradient: 'linear(to-r, #D74177, #FFE98A)',
                    bgClip: 'text'
                  }}
                >
                  {e.title}
                </Button>
              </SlideFade>
              )}
            </Flex>
          </Flex>
        </Flex>
      </Flex>
      <Divider></Divider>
      <Flex
        justifyContent='center'
      >
        <Text color='blackAlpha.800' fontWeight='semibold' width='1000px' my='35px'>
          Designed and Coded by Šimon Buchta
        </Text>
      </Flex>
      <Drawer onClose={onClose} isOpen={isOpen} size='full'>
        <DrawerOverlay />
        <DrawerContent>
          <DrawerCloseButton />
          <DrawerBody>
            <Flex
              height='100%'
              flexFlow='column'
              alignItems='center'
              justifyContent='center'
              gap='25px'
            >
              {sections.map((e, idx) => 
                <SlideFade in={true} offsetY="-20px" delay={(idx + 1) * 0.025 + 0.75}>
                  <Button 
                    ml='10px' 
                    size='lg' 
                    variant='ghost'
                    color='blackAlpha.700'
                    key={idx}
                    onClick={() => scrollToItem(e.id, 'start')}
                    _hover={{
                      bgGradient: 'linear(to-r, #D74177, #FFE98A)',
                      bgClip: 'text'
                    }}
                  >
                    {e.title}
                  </Button>
                </SlideFade>
              )}
            </Flex>
          </DrawerBody>
        </DrawerContent>
      </Drawer>
    </Box>
  );
}

export default MainLayout;