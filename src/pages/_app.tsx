import { ChakraProvider, ColorModeProvider, extendTheme } from '@chakra-ui/react';
import type { AppProps } from 'next/app';
import { DefaultSeo } from 'next-seo';
import { Chart, CategoryScale, LinearScale, BarElement } from 'chart.js';
import { useEffect } from 'react';
import 'aos/dist/aos.css';
import AOS from 'aos';

Chart.register(CategoryScale, LinearScale, BarElement);

function MyApp({ Component, pageProps }: AppProps) {
  useEffect(() => {
    AOS.init();
  }, []);

  const theme = extendTheme({
    styles: {
      global: () => ({
        body: {
          bg: '#ffffff',
        },
        colors: {
          myPink: {
            50: "#fde2e8",
            100: "#fbb6c9",
            200: "#f98ba0",
            300: "#f65f77",
            400: "#f4334e",
            500: "#d74177",
            600: "#b02c5e",
            700: "#891744",
            800: "#62032b",
            900: "#3e0011",
          },
        },
        breakpoints: {
          sm: "320px",
          md: "768px",
          lg: "1440px",
          xl: "1441px",
        },
      }),
    },
  });
  
    return (
      <ChakraProvider
        theme={theme}
      >
        <ColorModeProvider options={{ initialColorMode: "light", useSystemColorMode: false }}>
          <DefaultSeo
              titleTemplate="Netolismus"
              // additionalLinkTags={[ TODO
              //   {
              //     rel: 'icon',
              //     href: 'https://www.weddmate.com/icon.png',
              //   },
              // ]}
              defaultTitle="Netolismus"
              openGraph={{
                  title: 'Netolismus',
                  description:
                  'Netolismus',
              //   url: 'https://www.weddmate.com', TODO
              //   images: [
              //     {
              //       url: 'https://www.weddmate.com/images/og-image.png',
              //       alt: 'A logo with a heart, and a text saying "Plan your wedding with ease"',
              //       type: 'image/png',
              //       height: 800,
              //       width: 400,
              //     },
              //   ],
              }}
          />
          <style jsx global>{`
            body {
              margin: 0;
            }
          `}</style>
          <Component {...pageProps} />
      </ColorModeProvider>
    </ChakraProvider>
  );
}

export default MyApp;

declare global {
  interface Window {
    GA_INITIALIZED: boolean;
  }
}