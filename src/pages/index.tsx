import React, { useEffect } from 'react';
import MainLayout from '@/layouts/MainLayout';
import { Box, Flex, Text, useColorMode, HStack, Button, SlideFade } from '@chakra-ui/react';
import { TypeAnimation } from 'react-type-animation';
import { motion, useScroll, useTransform } from 'framer-motion';
import Aos from 'aos';

import * as FaIcons from 'react-icons/fa'
import CharCard, { CardType } from '@/components/CharCard';
import TypeComponent from '@/components/TypeComponent';
import MyChart from '@/components/MyChart';

const MotionBox = motion(Box);

const Index: React.FC = () => {
  const { colorMode } = useColorMode();
  const { scrollYProgress } = useScroll();
  const scale = useTransform(scrollYProgress, [0, 0.5], [0.5, 1]);
  const rotate1 = useTransform(scrollYProgress, [0, 0.5], [0, 360]);
  const rotate2 = useTransform(scrollYProgress, [0, 0.5], [180, -180]);
  const x = useTransform(scrollYProgress, [0, 0.5], ['100%', '0%']);

  const netolismTypes: CardType[] = [
    {
      title: 'Sociální Sítě',
      desc: 'Sociální sítě umožňují komunikaci a sdílení obsahu online. Přestože propojují lidi, mohou přinášet rizika jako kyberšikana. Je důležité vědět, jak se chránit a jak vytvářet zdravé online návyky.',
      icon: FaIcons.FaFacebookSquare,
    },
    {
      title: 'Počítačové hry',
      desc: 'Počítačové hry poskytují virtuální zážitky a dobrodružství. Mohou rozvíjet dovednosti, ale také mohou vést k závislosti. Důležité je hrát s mírou a rozpoznat potenciální rizika.',
      icon: FaIcons.FaGamepad,
    },
    {
      title: 'Online pornografie',
      desc: 'Online pornografie je snadno dostupná, ale může představovat závislost nebo problém pro některé lidi. Důležité je být si vědom svého konzumního chování a jeho dopadů na vztahy.',
      icon: FaIcons.FaEyeSlash,
    },
    {
      title: 'Online nakupování',
      desc: 'Online nákupy nabízejí pohodlí a flexibilitu, ale s sebou nesou rizika podvodů. Je klíčové informovat se o tom, kde nakupujete, a být opatrný při online transakcích.',
      icon: FaIcons.FaShoppingCart,
    },
    {
      title: 'Online vztahy',
      desc: 'V digitálním věku lze navazovat vztahy s lidmi z celého světa. Online interakce přinášejí nové možnosti, ale také výzvy. Opatrnost při sdílení informací je nezbytná.',
      icon: FaIcons.FaHeart,
    },
    {
      title: 'Internet',
      desc: 'Internet propojuje svět a umožňuje sdílení informací. Přináší výhody, ale také výzvy jako kybernetická bezpečnost a ochrana soukromí. Je důležité být online informován a opatrný.',
      icon: FaIcons.FaGlobe,
    }
  ]  
    
  const netolismChars: CardType[] = [
    {
      title: 'Význačnost',
      desc: 'Aktivita ovládá myšlení, cítění a chování člověka.',
      icon: FaIcons.FaExclamationCircle,
    },
    {
      title: 'Změny nálady',
      desc: 'Vyrovnávací strategie za účelem uklidnění se.',
      icon: FaIcons.FaThermometerHalf,
    },
    {
      title: 'Tolerance',
      desc: 'Potřeba více aktivity k dosažení stejného uspokojení.',
      icon: FaIcons.FaArrowUp,
    },
    {
      title: 'Relaps',
      desc: 'Tendence opakovat dřívější vzorce závislostního chování.',
      icon: FaIcons.FaRedo,
    },
    {
      title: 'Konflikt',
      desc: 'Závislost vyvolává problémy a narušuje vztahy.',
      icon: FaIcons.FaBalanceScale,
    }
  ]

  const steps: any[] = [
    'sociálnich sítích', 1000,
    'počítačových hrách', 1000,
    'online pornografii' , 1000,
    'online nakupování', 1000,
    'online vztazích', 1000,
    'internetu', 1000,
  ]

  useEffect(() => {
    Aos.init();
  }, []);

  const scrollToItem = (i: string, location: ScrollLogicalPosition) => {
    const item = document.querySelector(`#${i}`)
    if (!item) {
        return
    }
    item.scrollIntoView({ 
        behavior: 'smooth',
        inline: 'nearest',
        block: location,
    })
  }

  return (
    <MainLayout>
      <Box>
        <Box
          height='101vh'
          position='relative'
        >
          <Flex
            alignItems='center'
            justifyContent='center'
            height='100%'
            flexFlow='column'
          >
            <Flex
              fontSize='4xl'
              fontWeight='bold'
              gap='10px'
              flexFlow='column'
              alignItems='center'
              width='100%'
            >
              <SlideFade delay={0.1} in={true} offsetY="-20px">
                <Text
                  bgGradient='linear(to-r, #D74177, #FFE98A)'
                  bgClip='text'
                  fontWeight='extrabold'
                  fontSize={["2xl", "3xl", "6xl", "8xl"]}
                  lineHeight='shorter'
                >
                  Netolismus
                </Text>
              </SlideFade>
              <SlideFade delay={0.2} in={true} offsetY="-20px">
                <Text
                  fontWeight='extrabold'
                  fontSize={["2xl", "3xl", "6xl", "8xl"]}
                  lineHeight='shorter'
                  color='blackAlpha.800'
                >
                  je závislost na
                </Text>
              </SlideFade>
              <SlideFade delay={0.3} in={true} offsetY="-20px">
                <Text
                  fontWeight='extrabold'
                  fontSize={{
                    base: "3xl", // defaultní velikost písma
                    sm: "4xl",   // pro mobilní zařízení (320px a více)
                    md: "6xl",   // pro tablety (768px a více)
                    lg: "8xl",   // pro notebooky (1440px a více)
                    xl: "10xl"
                  }}
                  lineHeight='shorter'
                  color='blackAlpha.800'
                >
                  <TypeAnimation
                    sequence={steps}
                    repeat={Infinity}
                  />
                </Text>
              </SlideFade>
            </Flex>
            <Flex
              mt='25px'
              alignItems='center'
              flexFlow={{ base: 'column', lg: 'row' }}
              gap='10px'
            >
              <SlideFade in={true} delay={1.25} offsetX="-20px" offsetY="0">
                <Button 
                  width='100%'
                  colorScheme='blackAlpha'
                  size={{ base: 'sm', md: 'lg' }}
                  onClick={() => scrollToItem('characters', 'start')}
                >
                  JAK POZNÁM ŽE JSEM ZÁVSILÝ?
                </Button>
              </SlideFade>
              <SlideFade in={true} delay={1.25} offsetX="20px" offsetY="0">
                <Button 
                  colorScheme='blackAlpha'
                  variant='ghost'
                  size={{ base: 'sm', md: 'lg' }}
                  onClick={() => scrollToItem('help', 'start')}
                >
                  POTŘEBUJU POMOC!
                </Button>
              </SlideFade>

            </Flex>
          </Flex>
        </Box>
        <Box pos='relative' id='characters'>
          <Text
            color='blackAlpha.800'
            fontWeight='extrabold'
            fontSize='5xl'
            lineHeight='taller'
            textAlign='center'
            data-aos-delay='100'
            data-aos="fade"
            data-aos-easing="linear"
            data-aos-duration="250"
          >
            Znaky Netolismus
          </Text>
          <Flex
            bgGradient='linear(to-t, #D74177, #FFE98A)'
            alignItems='center'
            justifyContent='center'
            width='100%'
            py='50px'
            boxShadow='2xl'
            >
            <Flex
              gap='20px'
              width={{ base: '90%', xl: '1100px' }}
              justifyContent='center'
              flexWrap='wrap'
            >
              {
                netolismChars.map((card, idx) => 
                
                  <CharCard idx={idx} key={idx} card={card} />
                )
              }
            </Flex>
          </Flex>
          <Flex
            mt='50px'
            justifyContent='center'
            width='100%'
            id='types'
          >
            <Box
              width={{ base: '90%', xl: '1100px' }}
            >
              <Text
                color='blackAlpha.800'
                fontWeight='extrabold'
                fontSize='5xl'
                textAlign='left'
                data-aos="fade-right"
                data-aos-easing="linear"
                data-aos-delay='100'
                data-aos-duration="500"
              >
                Typy Netolismu
              </Text>
              <Text
                color='blackAlpha.600'
                fontWeight='semibold'
                fontSize='xl'
                textAlign='left'
                data-aos="fade-right"
                data-aos-easing="linear"
                data-aos-delay='100'
                data-aos-duration="500"
                mb='20px'
              >
                Netolismus je závislost na tzv. virtuálních drogách
              </Text>
              <Flex
                flexWrap='wrap'
                justifyContent={{ base: 'center', xl: 'space-between' }}
                rowGap='60px'
                columnGap='20px'
              >
                {netolismTypes.map((type, idx) => 
                  <TypeComponent type={type} key={idx} idx={idx}  />
                )}
              </Flex>
            </Box>
          </Flex>
          <Flex
            pt='50px'
            flexFlow='column'
            alignItems='center'
            mb='50px'
            id='stats'
          >
            <Text
                bgGradient='linear(to-r, #D74177, #FFE98A)'
                bgClip='text'
                fontWeight='extrabold'
                fontSize='5xl'
                lineHeight='shorter'
                data-aos-delay='100'
                data-aos="fade"
                data-aos-easing="linear"
                data-aos-duration="250"
            >
                Statistiky Netolismu ve Světě
            </Text>
            <Text
              data-aos-delay='100'
              data-aos="fade"
              data-aos-easing="linear"
              data-aos-duration="250"
              mb='20px'
            >
              Kolik % lidi je závislích na internetu ve světě. (Data jsou z roku 2014)
            </Text>
            <MyChart />
          </Flex>
        </Box>
      </Box>
    </MainLayout>
  );
}

export default Index;
