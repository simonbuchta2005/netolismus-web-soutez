import React from 'react';
import MainLayout from '@/layouts/MainLayout';
import { Box, Flex, Text, useColorMode } from '@chakra-ui/react';
import { TypeAnimation } from 'react-type-animation';
import Card, { CardType } from '@/components/Card';
import { ArrowRightIcon, CloseIcon, InfoIcon, SearchIcon } from '@chakra-ui/icons';
import { motion } from 'framer-motion'

const MotionBox = motion(Box)

const Index: React.FC = () => {
  const { colorMode } = useColorMode();
 
  const introCards: CardType[] = [
    {
      title: 'Definice',
      desc: 'Netolismus, někdy také nazývaný netholismus, je označení pro chorobnou závislost na internetu. Liší se od drogové závislosti tím, že nezávisí na látce, ale na chování či procesu, například potřebě být neustále online.',
      icon: InfoIcon
    },
    {
      title: 'Příčiny',
      desc: 'Mezi hlavní příčiny netolismu patří únik před stresující realitou, snaha navázat sociální kontakty online, či snaha dosáhnout pocitu úspěchu ve virtuálním světě.',
      icon: ArrowRightIcon
    },
    {
      title: 'Důsledky',
      desc: 'Netolismus může vést k fyzickým problémům jako jsou bolesti zad a krku, zhoršené vidění, a také k psychickým problémům jako jsou úzkost a deprese.',
      icon: CloseIcon
    },
  ]

  const steps: any[] = [
    'sociálnich sítích', 1000,
    'počítačových hrách', 1000,
    'online pornografii' , 1000,
    'online nakupování', 1000,
    'online vztazích', 1000,
    'internetu', 1000,
  ]

  return (
    <MainLayout>
      <Box>
        <Box
          height='101vh'
          position='relative'
        >
          <Flex
            alignItems='center'
            height='100%'
          >
            <Flex
              fontSize='4xl'
              fontWeight='bold'
              gap='10px'
              justifyContent='center'
              width='100%'
            >
              <Text
                bgGradient='linear(to-r, #D74177, #FFE98A)'
                bgClip='text'
              >
                Netolismus
              </Text>
              <Text>
                je závislost na
              </Text>
              <Text>
                <TypeAnimation
                  sequence={steps}
                  repeat={Infinity}
                />
              </Text>
            </Flex>
          </Flex>
        </Box>
        <Box pos='relative' >
          <MotionBox
            bgGradient='linear(to-r, #D74177, #FFE98A)'
            height='400px'
            borderRadius='3xl'
            pos='absolute'
            left='100px'
            top='-75px'
            boxShadow='lg'
            width='35%'
          />
          <MotionBox
            bgGradient='linear(to-r, #D74177, #FFE98A)'
            height='300px'
            borderRadius='3xl'
            pos='absolute'
            right='100px'
            top='350px'
            boxShadow='lg'
            width='35%'
            initial={{ x: '100%' }}
            whileInView={{ x: 0 }} 
          />
          <Flex
            width='100%'
            justifyContent='center'
          >
            <MotionBox
              w='70%'  
              h='500px'
              background='white'
              zIndex='5'
              boxShadow='lg'
              borderRadius='3xl'
              initial={{ x: '100%' }}
              whileInView={{ x: 0 }} 
              transition={{ delay: 0.25, duration: 0.75 }}
            >
              xxxx
            </MotionBox>
          </Flex>
          <Box
            width='100%'
            h='100vh'
          >
            x
          </Box>
        </Box>
      </Box>
    </MainLayout>
  );
}

export default Index;
