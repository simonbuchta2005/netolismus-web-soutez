import { Box } from '@chakra-ui/react';
import React from 'react';
import { Bar } from 'react-chartjs-2';

const data = {
  labels: ['Španělsko', 'Rumunsko', 'Polsko', 'Nizozemsko', 'Řecko', 'Německo', 'Island', 'Čína', 'Japonsko'],
  datasets: [
    {
      label: 'Odhadované procento závislých v ČR',
      data: [21.3, 16, 12, 11.4, 11, 9.7, 7.2, 11, 39],
      backgroundColor: [
        '#D74177',
      ],
    },
  ],
};

const options = {
  maintainAspectRatio: false,
  indexAxis: 'x' as const,
  elements: {
    bar: {
      borderWidth: 2,
    },
  },
  responsive: true,
  plugins: {
    legend: {
      position: 'right' as const,
    },
    title: {
      display: true,
      text: 'Odhadované procento závislých na internetu v ČR',
    },
  },
  scales: {
    y: {
      ticks: {
        callback: function(value: any) {
          return value + '%';  // Změní formát hodnot osy y na procenta
        }
      }
    }
  },
};

const MyChart = () => (
  <Box 
    width={{ base: '70%', xl: '1100px' }}  // Corrected from '80px%' to '80%'
    height={{ base: '200px', md: '300px', lg: '500px' }}
    data-aos='fade-down'
    data-aos-duration='500'
    data-aos-delay='200'
  >
    <Bar width='inherit' height='100%' data={data} options={options} />
  </Box>
);

export default MyChart;
