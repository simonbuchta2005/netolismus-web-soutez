import React from 'react'
import { Box, Button, Flex, Icon, Text } from '@chakra-ui/react'

export interface CardType {
  title: string,
  desc: string,
  icon: any,
}

interface CardProps {
  card: CardType,
}

const Card: React.FC<CardProps> = ({ card }) => {
  return (
    <Box
      boxShadow='xl'
      justifyContent='center'
      width='300px'
      height='300px'
      borderRadius='3xl'
      bg='white'
      padding='25px'
    >
      <Flex
        justifyContent='space-between'
      >
        <Text
          bgGradient='linear(to-r, #D74177, #FFE98A)'
          bgClip='text'
          fontWeight='700'
          textTransform='uppercase'
          fontSize='xl'
        >
          {card.title}
        </Text>
        <Icon 
          color='black'
          fontSize='25px'
          as={card.icon} 
        />
      </Flex>
      <Text
      >
        {card.desc}
      </Text>
    </Box>
  );
}

export default Card;