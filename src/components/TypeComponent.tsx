import React, { useEffect, useState } from 'react'
import { Box, Card, Divider, Flex, Heading, Icon, IconButton, SimpleGrid, Text, useToast } from "@chakra-ui/react";
export interface TypeComponentType {
    title: string,
    desc: string,
    icon: any,
  }
  
  interface TypeComponentProps {
    type: TypeComponentType,
    idx: number
  }
  

const TypeComponent: React.FC<TypeComponentProps> = ({ type, idx }) => {
    return (
        <Flex
            // width='500px'
            width={{ base: '100%', xl: '500px' }}
            gap='20px'
            alignItems='center'
            data-aos='fade-left'
            data-aos-duration='500'
            data-aos-delay={`${idx % 2 === 0 ? '150' : '300'}`}
        >
            <Box>
                <Text
                    bgGradient='linear(to-r, #D74177, #FFE98A)'
                    bgClip='text'
                    fontWeight='extrabold'
                    fontSize='5xl'
                    lineHeight='shorter'
                >
                    0{idx + 1}.
                </Text>
            </Box>
            <Box>
                <Flex
                    alignItems='center'
                    gap='10px'
                >
                    <Text
                        fontWeight='extrabold'
                        textTransform='uppercase'
                        color='blackAlpha.700'
                        fontSize='2xl'
                    >
                        {type.title}
                    </Text>
                    <Flex
                        bgGradient='linear(to-r, #D74177, #FFE98A)'
                        alignItems='center'
                        justifyContent='center'
                        p='5px'
                        borderRadius='md'
                    >
                        <Icon boxShadow='2xl' color='white' as={type.icon} />
                    </Flex>
                </Flex>
                <Text
                    fontSize='sm'
                    color='blackAlpha.600'
                >
                    {type.desc}
                </Text>
            </Box>
        </Flex>
    )
}

export default TypeComponent;